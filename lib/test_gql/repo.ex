defmodule TestGql.Repo do
  use Ecto.Repo,
    otp_app: :test_gql,
    adapter: Ecto.Adapters.Postgres
end
