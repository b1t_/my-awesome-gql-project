defmodule TestGqlWeb.Schema do

  use Absinthe.Schema

  object :user do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :age, non_null(:string)
  end

  query do
    field :test, :string do
      arg :msg, :string
      resolve fn _, %{msg: msg}, _ ->
        if msg == "ping", do: {:ok, "pong"}, else: {:error, "awaited ping"}
      end
    end
  end
end
