defmodule TestGqlWeb.PageController do
  use TestGqlWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
