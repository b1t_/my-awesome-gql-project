# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :test_gql,
  ecto_repos: [TestGql.Repo]

# Configures the endpoint
config :test_gql, TestGqlWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "UnsMrpsw+oHVY9942qdVXDE8CdoOXQXO3On7j0/hbY2X9byXO7W/K9q5yR/7/XP/",
  render_errors: [view: TestGqlWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: TestGql.PubSub,
  live_view: [signing_salt: "Mm5rFrdE"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
